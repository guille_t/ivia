exports.definition = {
	config: {
		columns: {
		    "id": "integer primary key autoincrement",
		    "path": "text",
		    "text": "text"
		},
		adapter: {
			type: "sql",
			collection_name: "photos"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};