exports.definition = {
	config: {
		columns: {
		    "id": "integer primary key autoincrement",
		    "current_date": "text",
		    "type": "integer",
		    "text": "text",
		    "value1": "text",
		    "value2": "text",
		    "value3": "text",
		    "value4": "text",
		    "h": "text",
		    "w": "text",
		    "w2": "text",
		    "f": "text",
		    "a": "text",
		    "result1": "text",
		    "result2": "text"
		},
		adapter: {
			type: "sql",
			collection_name: "forms"
		}
	},
	extendModel: function(Model) {
		_.extend(Model.prototype, {
			// extended functions and properties go here
		});

		return Model;
	},
	extendCollection: function(Collection) {
		_.extend(Collection.prototype, {
			// extended functions and properties go here
		});

		return Collection;
	}
};
