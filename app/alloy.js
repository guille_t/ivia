// The contents of this file will be executed before any of
// your view controllers are ever executed, including the index.
// You have access to all functionality on the `Alloy` namespace.
//
// This is a great place to do any initialization for your app
// or create any global variables/functions that you'd like to
// make available throughout your app. You can easily make things
// accessible globally by attaching them to the `Alloy.Globals`
// object. For example:
//
// Alloy.Globals.someGlobalFunction = function(){};

Alloy.Globals.Map = require('ti.map');

/*
 * Abriendo navegador web
 */
Alloy.Globals.getHTML = function(text) {
	var styles = '<style>'
		+ 'html { -webkit-text-size-adjust: none; color:#666; }'
		+ 'body { font-size: 14px; background: transparent; font-family: Helvetica; }'
		+ 'a, a:visited { color: #0084B4; }'
		+ 'img {float: left; margin: 0 10px 10px 0; }'
	+ '</style>';
	
	var header = '<html><head>'
		+ '<meta name="viewport" content="width=' + 300 + ', initial-scale=1, maximum-scale=1, user-scalable=false;">'
		+ styles
		+ '</head><body>';
		
	var footer = '</body></html>';
	var exp = /<a\b[^>]*href="([^"]*)"[^>]*>(.*?)<\/a>/ig;
	return header + text.replace(/^\s+/g,'').replace(/\s+$/g,'').replace(exp,'<a onclick="Ti.App.fireEvent(\'openWeb\', {url:\'$1\'});">$2</a>') + footer;
};

Ti.App.addEventListener('openWeb', function(e) {
	if (OS_IOS) {
		var url = e.url;
	} else {
		if (e.url.substr(-3) == 'pdf') {
			e.url = 'http://docs.google.com/viewer?embedded=true&url=' + e.url;
		} else {
			e.url = e.url;
		}
	}
	var t_web = Alloy.createController('web', {data:e.url}).getView();
	var win = Alloy.CFG.wins.pop();
	Alloy.CFG.wins.push(win);
	win.add(t_web);
	t_web.animate({opacity:1});	
});
/*
 * Fin del navegador web
 */


/*
 * Collections
 */
//Alloy.Collections.photos = Alloy.createCollection('photos');
