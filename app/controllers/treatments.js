$.header.setWindow($.win);
$.header.setTitle('Tratamientos fitosanitarios');

$.b1.top = '50dp';
$.b2.top = '50dp';
$.b1.left = '12dp';
$.b2.right = '12dp';

$.b1_image.image = '/images/treatment_prod.jpg';
$.b2_image.image = '/images/treatment_reco.jpg';

$.b1_lbl.text = 'Materias activas';
$.b2_lbl.text = 'Recomendaciones';

var getData = require('get_treatments');

$.b1.addEventListener('singletap', function(e) {
	$.table.data = [];
	$.loading.show();
	$.table.hide();
	$.table_title_text.text = $.b1_lbl.text;
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	new getData(setData, 'productos');
	$.table.top = '230dp';
	$.caldo_view.hide();
});
$.b2.addEventListener('singletap', function(e) {
	$.table.data = [];
	$.loading.show();
	$.table.hide();
	$.table_title_text.text = $.b2_lbl.text;
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	new getData(setData, 'recomendaciones');
	$.table.top = '280dp';
	$.caldo_view.show();
});

function setData(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.loading.hide();
		return;
	}
	
	var rows = [];
	
	for (var i = 0; i < data.length; i ++) {

		var row = Ti.UI.createTableViewRow({
			height:'40dp',
			title:data[i].title,
			color:'#000',
			font:{fontSize:Alloy.CFG.fontSize, fontWeight:'normal', fontFamily:Alloy.CFG.fontFamily}
		});
		
		rows.push(row);
		
		row._data = data[i];
		
	}
	
	$.table.appendRow(rows);
	
	$.loading.hide();
	$.table.show();
	
}

$.table.addEventListener('click', function(e) {
	
	var view = Alloy.createController('treatments/view', e.row._data).getView();
	Alloy.CFG.wins.push(view);
	view.open({left:0});
	
});

$.caldo.addEventListener('click', function() {
	$.caldo.opacity = .3;
	$.caldo.animate({opacity:1});
	
	var caldo = Alloy.createController('caldo').getView();
	Alloy.CFG.wins.push(caldo);
	caldo.open({left:0});
	
});
