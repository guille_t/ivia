$.header.setWindow($.win);
$.header.setTitle('Enemigos naturales');

$.b1.top = '50dp';
$.b2.top = '50dp';
$.b1.left = '12dp';
$.b2.right = '12dp';

$.b1_image.image = '/images/cat3.png';
$.b2_image.image = '/images/enemies_p.jpg';

$.b1_lbl.text = 'Depredadores';
$.b2_lbl.text = 'Parasitoides';

var getData = require('get_enemies');

$.b1.addEventListener('singletap', function(e) {
	$.table.data = [];
	$.loading.show();
	$.table.hide();
	$.table_title_text.text = $.b1_lbl.text;
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	new getData(setData, 'depredadores');
});
$.b2.addEventListener('singletap', function(e) {
	$.table.data = [];
	$.loading.show();
	$.table.hide();
	$.table_title_text.text = $.b2_lbl.text;
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	new getData(setData, 'parasitoides');
});

function setData(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.loading.hide();
		return;
	}
	
	var rows = [];
	
	for (var i = 0; i < data.length; i ++) {

		var row = Ti.UI.createTableViewRow({
			height:'40dp',
			title:data[i].title,
			color:'#000',
			font:{fontSize:Alloy.CFG.fontSize, fontWeight:'normal', fontFamily:Alloy.CFG.fontFamily}
		});
		
		rows.push(row);
		
		row._data = data[i];
		
	}
	
	$.table.appendRow(rows);
	
	$.loading.hide();
	$.table.show();
	
}

$.table.addEventListener('click', function(e) {
	
	var view = Alloy.createController('enemies/view', e.row._data).getView();
	Alloy.CFG.wins.push(view);
	view.open({left:0});
	
});
