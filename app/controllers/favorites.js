var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle('Favoritos');

var getFavorites = require('get_favorites');
new getFavorites(setData);

$.reload.addEventListener('singletap', function() {
	$.reload.hide();
	$.loading.show();
	new getFavorites(setData);
});

$.loading.show();

function setData(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.reload.show();
		$.loading.hide();
		return;
	}
	
	if (data.length == 0) {
		var ok = Ti.UI.createAlertDialog({
			title:'Sin favoritos',
			message:'No has añadido ningún favorito todavía. Lo puedes hacer en el icono en forma de estrella dentro de cada ficha de cada artículo.',
			ok:'Vale'
		});
		ok.show();
		ok.addEventListener('click', function() {
			$.win.close({left:'100%'});
		});
	}
	
	var rows = [];
	for (var i = 0; i < data.length; i ++) {
		var row = Alloy.createController('row', data[i]).getView();
		rows.push(row);
	}
	
	$.table.data = rows;
	
	$.loading.hide();
	
	$.bg_logo.hide();
	
}

$.table.addEventListener('click', function(e) {

	e.row._row_view.opacity = .3;
	e.row._row_view.animate({opacity:1});
	
	var win = Alloy.createController('article', {title:e.row._title, id:e.row._id, post:0}).getView();
	
	Alloy.CFG.wins.push(win);
	win.open({left:0});
	
});
