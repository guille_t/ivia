var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.a.title);

if (args.item_id) {
	$.title.text = 'Integrales térmicas';
	$.graph_text_1.text = '';
	$.scrollview.remove($.hidde1);
	$.scrollview.remove($.hidde2);
	$.scrollview.remove($.hidde3);
} else {
	$.scrollview.remove($.hidde4);
	$.scrollview.remove($.hidde5);
	$.scrollview.remove($.hidde6);
	$.scrollview.remove($.hidde7);
}

$.header.setBgColor('#EBEBEB', '#333');

$.loading.show();

Ti.Gesture.addEventListener('orientationchange', function(e) {
	$.image.width = Ti.Platform.displayCaps.platformWidth;
});

var client = Ti.Network.createHTTPClient({
	onload: function() {
		var result = JSON.parse(this.responseText);
		Ti.API.info('RESULT: ' + this.responseText);
		if (result.status == 'ok') {
			if (result.text1 && result.text2) {
				$.text1.text = result.text1;
				$.text2.text = result.text2;
				if (result.date) {
					$.trat1.text += ' ' + result.date;
				}
				if (result.date2) {
					$.trat2.text += ' ' + result.date2;
				} else {
					if (!result.t2) {
						$.scrollview.remove($.hidde7);
					}
				}
			}
			//$.image.image = result.data;
			var client2 = Ti.Network.createHTTPClient({
				onload: function() {
					$.image.image = this.responseData;
				},
				timeout: 10000
			});
			client2.open('GET', OS_IOS ? result.data : result.data_android);
			client2.send();
		}
		$.loading.hide();
	},
	onerror: function(e) {
		$.loading.hide();
	},
	timeout: 10000
});
args.item_id = args.item_id || '';
client.open('GET', Alloy.CFG.url + 'app/alternaria.php?id_estacion=' + args.a._id + '&item_id=' + args.item_id);
client.send();

$.link.addEventListener('singletap', function() {
	var t_web = Alloy.createController('web', {data:'http://riegos.ivia.es'}).getView();
	var win = Alloy.CFG.wins.pop();
	Alloy.CFG.wins.push(win);
	win.add(t_web);
	t_web.animate({opacity:1});	
});
