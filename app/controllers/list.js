var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.title);

var getSubcats = require('subcats');
new getSubcats(setSubcats, args.id);

$.reload.addEventListener('singletap', function() {
	$.reload.hide();
	$.loading.show();
	new getSubcats(setSubcats, args.id);
});

$.loading.show();

function setSubcats(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.reload.show();
		$.loading.hide();
		return;
	}
	
	var rows = [];
	for (var i = 0; i < data.length; i ++) {
		var row = Alloy.createController('row', data[i]).getView();
		rows.push(row);
	}
	
	$.table.data = rows;
	
	$.loading.hide();
	
	$.bg_logo.hide();
	$.bg_gip.hide();
	
}

$.table.addEventListener('click', function(e) {

	e.row._row_view.opacity = .3;
	e.row._row_view.animate({opacity:1});
	
	if (e.source.id == 'left_tab') {
		var win = Alloy.createController('article', {title:e.row._title, id:e.row._id, post:0}).getView();
	} else if (e.source.id == 'center_tab') {
		var win = Alloy.createController('article', {title:e.row._title, id:e.row._id, post:1}).getView();
	} else if (e.source.id == 'right_tab') {
		var win = Alloy.createController('article', {title:e.row._title, id:e.row._id, post:2}).getView();
	} else {
		var win = Alloy.createController('article', {title:e.row._title, id:e.row._id, post:0}).getView();
	}
	
	Alloy.CFG.wins.push(win);
	win.open({left:0});
	
});
