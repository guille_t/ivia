$.header.setWindow($.win);
$.header.setTitle('Noticias');

$.header.setBgColor('#EBEBEB', '#333');

var getNews = require('get_news');
new getNews(setNews);

$.reload.addEventListener('singletap', function() {
	$.reload.hide();
	$.loading.show();
	new getNews(setNews);
});

$.loading.show();

function setNews(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.reload.show();
		$.loading.hide();
		return;
	}
	
	var rows = [];
	
	for (var i = 0; i < data.length; i ++) {

		var row = Alloy.createController('news/row', data[i]).getView();
		
		rows.push(row);
		
		row._data = data[i];
		
	}
	
	$.table.appendRow(rows);
	
	$.loading.hide();
	$.table.show();
	
}

$.table.addEventListener('click', function(e) {
	if (OS_IOS) {
		e.row._view.opacity = .3;
		e.row._view.animate({opacity:1});
	}
	var win = Alloy.createController('news/view', e.row._data).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});
