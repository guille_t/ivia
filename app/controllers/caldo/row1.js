var args = arguments[0] || {};

$.text.text = args.text;
$.h.text = 'Altura del árbol: ' + args.h + ' m';
$.w.text = 'Anchura del árbol (a trabés de la fila): ' + args.w + ' m';
$.w2.text = 'Anchura del árbol (a lo largo de la fila): ' + args.w2 + ' m';
$.f.text = 'Anchura de la calle: ' + args.f + ' m';
$.a.text = 'Distancia entre árboles: ' + args.a + ' m';

$.value1.text = 'Densidad foliar: ' + args.value1;
$.value2.text = 'Poda: ' + args.value2;
$.value3.text = 'Plaga/enfermedad: ' + args.value3;
$.value4.text = 'Producto: ' + args.value4;

$.result1.text = 'Volumen mínimo: ' + args.result1 + ' l/ha';
$.result2.text = 'Volumen máximo: ' + args.result2 + ' l/ha';

$.row._share = $.text.text.replace(/\n/g, '<br />') + '<br /><br />' +
	$.h.text + '<br />' +
	$.w.text + '<br />' +
	$.w2.text + '<br />' +
	$.f.text + '<br />' +
	$.a.text + '<br />' +
	$.value1.text + '<br />' +
	$.value2.text + '<br />' +
	$.value3.text + '<br />' +
	$.value4.text + '<br /><br />' +
	$.result1.text + '<br />' +
	$.result2.text;

$.row.rowId = args.id;
