var args = arguments[0] || {};

var hint = Ti.UI.createLabel({
	text: $.textarea.hintText,
	color:'#BBB',
	font:{fontSize:Alloy.CFG.fontSize, fontFamily:Alloy.CFG.fontFamily},
	top:'10dp',
	left:'10dp',
	visible:true
});
$.textarea.add(hint);
$.textarea.addEventListener('change', function() {
	if ($.textarea.value == '') {
		hint.show();
	} else {
		hint.hide();
	}
});

Alloy.Collections.forms.fetch();

var title = '';

switch (args.type) {
	case 1:
		title = 'Recomendación de volumen';
		$.saving.text = 'Datos introducidos\n\n'
			+ 'Altura del árbol: ' + args.h + ' m\n'
			+ 'Ancho del árbol (a través de la fila): ' + args.w + ' m\n'
			+ 'Ancho del árbol (a lo largo de la fila): ' + args.w2 + ' m\n'
			+ 'Ancho de la calle: ' + args.f + ' m\n'
			+ 'Distancia entre árboles: ' + args.a + ' m\n\n'
			+ 'Densidad foliar: ' + args.value1 + '\n\n'
			+ 'Poda: ' + args.value2 + '\n\n'
			+ 'Plaga/enfermedad: ' + args.value3 + '\n\n'
			+ 'Producto: ' + args.value4 + '\n\n'
			+ 'Volumen mínimo: ' + args.result1 + ' l/ha\n'
			+ 'Volumen máximo: ' + args.result2 + ' l/ha';
		break;
	case 2:
		title = 'Caudal de boquillas';
		$.saving.text = 'Datos introducidos\n\n'
			+ 'Volumen del caldo: ' + args.value1 + ' l/ha\n'
			+ 'Velocidad de avance: ' + args.value2 + ' km/h\n'
			+ 'Ancho de la calle: ' + args.value3 + ' m\n'
			+ 'Nº boquillas abiertas: ' + args.value4 + '\n\n'
			+ 'Caudal boquilla: ' + args.result1 + ' l/min';
		break;
	case 3:
		title = 'Cálculo de volúmenes';
		$.saving.text = 'Datos introducidos\n\n'
			+ 'Velocidad de avance: ' + args.h + ' km/h\n'
			+ 'Ancho de la calle: ' + args.w + ' m\n\n';
			
		var aux1 = args.value1.split('[-]');
		$.saving.text += 'Nombre boquilla 1: ' + aux1[0] + '\n'
			+ 'Nº boquillas 1: ' + aux1[1] + '\n'
			+ 'Caudal boquilla 1: ' + aux1[2] + '\n\n';
			
		if (args.value2 != '[-][-]') {
			var aux2 = args.value2.split('[-]');
			$.saving.text += 'Nombre boquilla 2: ' + aux2[0] + '\n'
				+ 'Nº boquillas 2: ' + aux2[1] + '\n'
				+ 'Caudal boquilla 2: ' + aux2[2] + '\n\n';
		}	
		
		if (args.value3 != '[-][-]') {
			var aux3 = args.value3.split('[-]');
			$.saving.text += 'Nombre boquilla 3: ' + aux3[0] + '\n'
				+ 'Nº boquillas 3: ' + aux3[1] + '\n'
				+ 'Caudal boquilla 3: ' + aux3[2] + '\n\n';
		}	
		
		if (args.value4 != '[-][-]') {
			var aux4 = args.value4.split('[-]');
			$.saving.text += 'Nombre boquilla 4: ' + aux4[0] + '\n'
				+ 'Nº boquillas 4: ' + aux4[1] + '\n'
				+ 'Caudal boquilla 4: ' + aux4[2] + '\n\n';
		}
		
		$.saving.text += 'Volumen aplicado: ' + args.result1 + ' l/ha';
		
		break;
}

$.cancel.addEventListener('click', function(e) {
	$.win.close({left:'100%'});
});
$.accept.addEventListener('click', function(e) {
	if (!$.textarea.value) {
		return;
	}
	var date = new Date();
	args.text = title + '\n' + date.getDate() + '-' + zerofill(date.getMonth() + 1) + '-' + date.getFullYear() + ' ' + zerofill(date.getHours()) + ':' + zerofill(date.getMinutes()) + '\n' + $.textarea.value;
	var form = Alloy.createModel('forms', args);
	form.save();
	Alloy.Collections.forms.fetch();
	$.win.close({left:'100%'});
});

function zerofill(aux) {
	return aux < 10 ? '0' + aux : aux;
}
