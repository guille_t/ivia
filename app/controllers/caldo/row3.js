var args = arguments[0] || {};

$.text.text = args.text;
$.h.text = 'Velocidad de avance: ' + args.h + ' km/h';
$.w.text = 'Ancho de la calle: ' + args.w + ' m';

var aux1 = args.value1.split('[-]');

$.value1.text = 'Nombre boquilla 1: ' + aux1[0] + '\n'
	+ 'Nº boquillas 1: ' + aux1[1] + '\n'
	+ 'Caudal boquilla 1: ' + aux1[2];
	
if (args.value2 != '[-][-]') {
	var aux2 = args.value2.split('[-]');
	$.value2.text = 'Nombre boquilla 2: ' + aux2[0] + '\n'
		+ 'Nº boquillas 2: ' + aux2[1] + '\n'
		+ 'Caudal boquilla 2: ' + aux2[2];
} else {
	$.row.remove($.value2);
}

if (args.value3 != '[-][-]') {
	var aux3 = args.value3.split('[-]');
	$.value3.text = 'Nombre boquilla 3: ' + aux3[0] + '\n'
		+ 'Nº boquillas 3: ' + aux3[1] + '\n'
		+ 'Caudal boquilla 3: ' + aux3[2];
} else {
	$.row.remove($.value3);
}

if (args.value4 != '[-][-]') {
	var aux4 = args.value4.split('[-]');
	$.value4.text = 'Nombre boquilla 4: ' + aux4[0] + '\n'
		+ 'Nº boquillas 4: ' + aux4[1] + '\n'
		+ 'Caudal boquilla 4: ' + aux4[2];
} else {
	$.row.remove($.value4);
}

$.result.text = 'Volumen aplicado: ' + args.result1 + ' l/ha';

$.row._share = $.text.text.replace(/\n/g, '<br />') + '<br /><br />' +
	$.h.text + '<br />' +
	$.w.text + '<br /><br />' +
	$.value1.text.replace(/\n/g, '<br />') + '<br /><br />';

if (args.value2 != '[-][-]') {
	$.row._share += $.value2.text.replace(/\n/g, '<br />') + '<br /><br />';
}
if (args.value3 != '[-][-]') {
	$.row._share += $.value3.text.replace(/\n/g, '<br />') + '<br /><br />';
}
if (args.value4 != '[-][-]') {
	$.row._share += $.value4.text.replace(/\n/g, '<br />') + '<br /><br />';
}

$.row._share += $.result.text;

$.row.rowId = args.id;