var args = arguments[0] || {};

Alloy.Collections.forms.fetch();

$.header.setWindow($.win);
$.header.setTitle('Datos guardados');
$.header.setBgColor('#EBEBEB', '#333');

var table = Alloy.createCollection("forms");
table.fetch({query:"SELECT * FROM forms"});

var rows = [];

for (var i = 0; i < table.length; i ++) {
	
	var rowData = {
		id:table.at(i).get('id'),
		text:table.at(i).get('text'),
		value1:table.at(i).get('value1'),
		value2:table.at(i).get('value2'),
		value3:table.at(i).get('value3'),
		value4:table.at(i).get('value4'),
		h:table.at(i).get('h'),
		w:table.at(i).get('w'),
		w2:table.at(i).get('w2'),
		f:table.at(i).get('f'),
		a:table.at(i).get('a'),
		result1:table.at(i).get('result1'),
		result2:table.at(i).get('result2')
	};
	
	var row = Alloy.createController('caldo/row' + table.at(i).get('type'), rowData).getView();
	
	rows.push(row);
	
}

$.table.appendRow(rows);

$.table.addEventListener('click', function(e) {
	
	if (e.source._delete) {
		
		var confirm = Ti.UI.createAlertDialog({
			title:'Eliminar elemento',
			message:'¿Seguro que deseas eliminar este elemento?',
			buttonNames:['Sí', 'No'],
			cancel:1
		});
		
		confirm.show();
		
		confirm.addEventListener('click', function(e2) {
			
			if (e2.source.cancel !== e2.index) {
				var table = Alloy.createCollection("forms");
				table.fetch({query:"SELECT * FROM forms where id = " + e.row.rowId });
			    if (table.length > 0) {
					table.at(0).destroy();
					Alloy.Collections.forms.fetch();
					$.table.deleteRow(e.row);
				}
			}
			
		});
		
	} else if (e.source._share) {
		
		Ti.UI.createEmailDialog({
			subject:'Te han compartido este cálculo',
			html:true,
			toRecipients:null,
			messageBody:e.row._share
		}).open();
		
	} else {
		
	}
	
});
