var args = arguments[0] || {};

$.text.text = args.text;
$.value1.text = 'Volumen del caldo: ' + args.value1 + ' l/ha';
$.value2.text = 'Velocidad de avance: ' + args.value2 + ' km/h';
$.value3.text = 'Ancho de la calle: ' + args.value3 + ' m';
$.value4.text = 'Nº boquillas abiertas: ' + args.value4;

$.result.text = 'Caudal boquilla: ' + args.result1 + ' l/min';

$.row.rowId = args.id;

$.row._share = $.text.text.replace(/\n/g, '<br />') + '<br /><br />' +
	$.value1.text + '<br />' +
	$.value2.text + '<br />' +
	$.value3.text + '<br />' +
	$.value4.text + '<br /><br />' +
	$.result.text;
