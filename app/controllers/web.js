var args = arguments[0] || {};

$.web.url = args.data;

$.loader.show();
$.reload_web.hide();

$.close.addEventListener('click', function() {
	$.view.animate({opacity:0}, function() {
		$.view.parent.remove($.view);
	});
});

$.web.addEventListener('load', function() {

	$.loader.hide();
	$.reload_web.show();

	if (!$.web.canGoBack()) {
		$.back.enabled = false;
	} else {
		$.back.enabled = true;
	}
	if (!$.web.canGoForward()) {
		$.fwd.enabled = false;
	} else {
		$.fwd.enabled = true;
	}

});

$.back.addEventListener('click', function() {
	if ($.web.canGoBack()) {
		$.web.goBack();
	}
});

$.fwd.addEventListener('click', function() {
	if ($.web.canGoForward()) {
		$.web.goForward();
	}
});

$.reload_web.addEventListener('click', function() {
	$.web.reload();
});

$.web.addEventListener('beforeload', function() {
	$.loader.show();
	$.reload_web.hide();
});

$.open.addEventListener('click', function() {
	Ti.Platform.openURL($.web.url);
});
