var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.title);

$.header.setBgColor('#EBEBEB', '#333');

$.win.add(Ti.UI.createWebView({
	top:'50dp',
	backgroundColor:'transparent',
	html: Alloy.Globals.getHTML(args.text) 
}));