var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.title);

$.header.setBgColor('#EBEBEB', '#333');

// $.webview.html = Alloy.Globals.getHTML(args.text);

var wv = Ti.UI.createWebView({
    html: Alloy.Globals.getHTML(args.text),
	top:'50dp',
    height:Ti.UI.SIZE,
    backgroundColor:'transparent'
});
$.win.add(wv);