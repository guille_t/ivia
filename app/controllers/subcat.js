var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.title);

var getSubcats = require('subcats');
new getSubcats(setSubcats, args.id);

$.reload.addEventListener('singletap', function() {
	$.reload.hide();
	$.loading.show();
	new getSubcats(setSubcats, args.id);
});

$.loading.show();

function setSubcats(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.reload.show();
		$.loading.hide();
		return;
	}
	
	var num_rows = -1;
	
	var w = 100;
	var l1 = 9;
	var l2 = 110;
	var l3 = 211;
	
	if (Ti.Platform.displayCaps.platformWidth > 640) {
		w *= 1.125;
		l1 *= 1.125;
		l2 *= 1.125;
		l3 *= 1.125;
	}
	
	for (var i = 0; i < data.length; i ++) {
		
		if (i % 3 === 0) {
			var left = l1 + 'dp';
			num_rows ++;
		} else if (i % 3 === 1) {
			var left = l2 + 'dp';
		} else {
			var left = l3 + 'dp';
		}
		
		var top = (num_rows * (w + 1)) + 'dp';
		
		var view = Alloy.createController('square', {left:left, top:top, data:data[i]}).getView();
		
		view.width = view.height = w + 'dp';
		
		$.scrollview.add(view);
		
		view.addEventListener('singletap', function(e) {
			e.source.opacity = .3;
			e.source.animate({opacity:1});
			if (args.id == 9) {
				var subcat = Alloy.createController('article', {id:e.source._id, title:e.source._title, post:0}).getView();
				Alloy.CFG.wins.push(subcat);
				subcat.open({left:0});
			}
			if (args.id == 10) {
				var subcat = Alloy.createController('list', {id:e.source._id, title:e.source._title}).getView();
				Alloy.CFG.wins.push(subcat);
				subcat.open({left:0});
			}
		});
		
	}
	
	$.loading.hide();
	$.bg_logo.hide();
	$.bg_gip.hide();
	
}
