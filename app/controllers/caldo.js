$.header.setWindow($.win);
$.header.setTitle('Volumen de aplicación y ajuste del equipo');

$.header.setBgColor('#EBEBEB', '#333');

if (Ti.Platform.displayCaps.platformWidth > 640) {
	$.volumen_aplicacion.height = '150dp';
	$.eleccion_boquillas.height = '150dp';
	$.volumen_turbo.height = '150dp';
}

function error() {
	Ti.UI.createAlertDialog({
		title:'Error de conexión',
		message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
		ok:'Vale'
	}).show();
}

var data1 = [
	Ti.UI.createPickerRow({title:'Media (Clementinas, Navel, Clemenules, Nadorcott, Nova, Orri, Whasington, Lane late, Marisol, Oronules)', value:2, index:0}),
	Ti.UI.createPickerRow({title:'Baja (Satsumas, Owari, Okitsu, Limoneros, Limón Fino)', value:1, index:1}),
	Ti.UI.createPickerRow({title:'Alta (Fortune, Garbí, Moncada)', value:3, index:2})
];
var data2 = [
	Ti.UI.createPickerRow({title:'Normal', value:2, index:0}),
	Ti.UI.createPickerRow({title:'Severa', value:1, index:1}),
	Ti.UI.createPickerRow({title:'Sin podar', value:3, index:2})
];

var data3 = [];
var data4 = [];

var data5 = [
	Ti.UI.createPickerRow({title:'0', value:0, index:0}),
	Ti.UI.createPickerRow({title:'1', value:1, index:1}),
	Ti.UI.createPickerRow({title:'2', value:2, index:2}),
	Ti.UI.createPickerRow({title:'3', value:3, index:3}),
	Ti.UI.createPickerRow({title:'4', value:4, index:4})
]; // Número de tipo de boquilla

var client = Ti.Network.createHTTPClient({
	onload: function() {
		var result = JSON.parse(this.responseText);
		Ti.API.info('RESULT: ' + this.responseText);
		if (result.status == 'ok') {
			data3 = [];
			for (var i in result.data) {
				data3.push(Ti.UI.createPickerRow({title:result.data[i].title, value:result.data[i].value, index:i}));
			}
			if (OS_ANDROID) {
				$.picker_tpl.add(data3);
			}
		} else {
			error();
		}
	},
	onerror: function(e) {
		error();
	},
	timeout: 10000
});
client.open('GET', Alloy.CFG.url + 'app/alternaria_data.php?type=plagas');
client.send();

var client = Ti.Network.createHTTPClient({
	onload: function() {
		var result = JSON.parse(this.responseText);
		Ti.API.info('RESULT: ' + this.responseText);
		if (result.status == 'ok') {
			data4 = [];
			for (var i in result.data) {
				data4.push(Ti.UI.createPickerRow({title:result.data[i].title, value:result.data[i].value, index:i}));
			}
			if (OS_ANDROID) {
				$.picker_tpr.add(data4);
			}
		} else {
			error();
		}
	},
	onerror: function(e) {
		error();
	},
	timeout: 10000
});
client.open('GET', Alloy.CFG.url + 'app/alternaria_data.php?type=productos');
client.send();

var value1 = null,
	value2 = null,
	value3 = null,
	value4 = null,
	value5 = null,
	index1 = 0,
	index2 = 0,
	index3 = 0,
	index4 = 0,
	index5 = 0;

if (OS_IOS) {
	
	var picker_view = Ti.UI.createView({
		borderColor:'#333',
		borderWidth:1,
		height:251,
		bottom:-251,
		zIndex:130
	});
	
	var cancel = Ti.UI.createButton({
		title:'Cancelar',
		style:Ti.UI.iOS.SystemButtonStyle.BORDERED
	});

	var done = Ti.UI.createButton({
		title:'Aceptar',
		style:Ti.UI.iOS.SystemButtonStyle.DONE
	});

	var spacer1 = Ti.UI.createButton({
		systemButton:Ti.UI.iOS.SystemButton.FLEXIBLE_SPACE
	});
	var title = Ti.UI.createLabel({
		color:Ti.Platform.version >= '7' ? '#333' : '#FFF'
	});
	var spacer2 = Ti.UI.createButton({
		systemButton:Ti.UI.iOS.SystemButton.FLEXIBLE_SPACE
	});

	var toolbar = Ti.UI.iOS.createToolbar({
		top:0,
		items:[cancel,spacer1,title,spacer2,done]
	});

	var slide_in = Ti.UI.createAnimation({bottom:0});
	var slide_out = Ti.UI.createAnimation({bottom:-251});

	picker_view.add(toolbar);

	var picker_showed = 0;
	
	done.addEventListener('click', function() {
		if (current_text.id == 'nb') {
			switch (picker.getSelectedRow(0).value) {
				case 0:
					$.block1.height = 0;
					$.block2.height = 0;
					$.block3.height = 0;
					$.block4.height = 0;
					break;
				case 1:
					$.block1.height = Ti.UI.SIZE;
					$.block2.height = 0;
					$.block3.height = 0;
					$.block4.height = 0;
					break;
				case 2:
					$.block1.height = Ti.UI.SIZE;
					$.block2.height = Ti.UI.SIZE;
					$.block3.height = 0;
					$.block4.height = 0;
					break;
				case 3:
					$.block1.height = Ti.UI.SIZE;
					$.block2.height = Ti.UI.SIZE;
					$.block3.height = Ti.UI.SIZE;
					$.block4.height = 0;
					break;
				case 4:
					$.block1.height = Ti.UI.SIZE;
					$.block2.height = Ti.UI.SIZE;
					$.block3.height = Ti.UI.SIZE;
					$.block4.height = Ti.UI.SIZE;
					break;
			}
			current_text.text = current_text.txt + ' -> ' + picker.getSelectedRow(0).title;
			eval('index' + current_picker + ' = ' + picker.getSelectedRow(0).index);
			picker_view.animate(slide_out);
		} else {
			current_text.text = current_text.txt + ' -> ' + picker.getSelectedRow(0).title;
			picker_view.animate(slide_out);
			if (typeof picker.getSelectedRow(0).value != 'string') {
				eval('value' + current_picker + ' = ' + picker.getSelectedRow(0).value);
			} else {
				eval('title' + current_picker + ' = "' + picker.getSelectedRow(0).title + '"');
				eval('value' + current_picker + ' = "' + picker.getSelectedRow(0).value + '"');
			}
			eval('index' + current_picker + ' = ' + picker.getSelectedRow(0).index);
		}
	});
	cancel.addEventListener('click', function() {
		picker_view.animate(slide_out);
	});
			
	var picker = Ti.UI.createPicker({
		backgroundColor:'white',
		top:43,
		selectionIndicator:true
	});
	picker.add(data1);
	picker_view.add(picker);
	
	$.win.add(picker_view);
	
	var current_text = null;
	
	$.df1.addEventListener('singletap', function() {
		current_text = this;
		title.text = this.txt;
		fill(1);
	});
	$.df2.addEventListener('singletap', function() {
		current_text = this;
		title.text = this.txt;
		fill(2);
	});
	$.tpl.addEventListener('singletap', function() {
		current_text = this;
		title.text = this.txt;
		fill(3);
	});
	$.tpr.addEventListener('singletap', function() {
		current_text = this;
		title.text = this.txt;
		fill(4);
	});
	$.nb.addEventListener('singletap', function() {
		current_text = this;
		fill(5);
	});
	
	var current_picker = null;

} else {
	$.picker_df1.add(data1);
	$.picker_df2.add(data2);
	//$.picker_tpl.add(data3);
	//$.picker_tpr.add(data4);
	$.picker_nb.add(data5);
	$.picker_nb.addEventListener('change', function(e) {
		switch ($.picker_nb.getSelectedRow(0).value) {
			case 0:
				$.block1.height = 0;
				$.block2.height = 0;
				$.block3.height = 0;
				$.block4.height = 0;
				break;
			case 1:
				$.block1.height = Ti.UI.SIZE;
				$.block2.height = 0;
				$.block3.height = 0;
				$.block4.height = 0;
				break;
			case 2:
				$.block1.height = Ti.UI.SIZE;
				$.block2.height = Ti.UI.SIZE;
				$.block3.height = 0;
				$.block4.height = 0;
				break;
			case 3:
				$.block1.height = Ti.UI.SIZE;
				$.block2.height = Ti.UI.SIZE;
				$.block3.height = Ti.UI.SIZE;
				$.block4.height = 0;
				break;
			case 4:
				$.block1.height = Ti.UI.SIZE;
				$.block2.height = Ti.UI.SIZE;
				$.block3.height = Ti.UI.SIZE;
				$.block4.height = Ti.UI.SIZE;
				break;
		}
	});
}

function fill(num) {
	current_picker = num;
	if (picker.columns[0]) {
		var _col = picker.columns[0];
		var len = _col.rowCount;
		for (var x = len-1; x >= 0; x--) {
			var _row = _col.rows[x];
			_col.removeRow(_row);
		}
		picker.reloadColumn(_col);
	}
	picker.add(eval('data' + num));
	picker.setSelectedRow(0, eval('index' + num));
	picker_view.animate(slide_in);
}

var aux = 1.2,
	remote1 = 3,
	remote2 = 3.5,
	remote3 = 4,
	remote4 = 4,
	remote5 = 4.5,
	remote6 = 5,
	remote7 = 5,
	remote8 = 5.5,
	remote9 = 6;
var client = Ti.Network.createHTTPClient({
	onload: function() {
		var result = this.responseText.split('\n');
		aux = result[0];
		remote1 = result[1];
		remote2 = result[2];
		remote3 = result[3];
		remote4 = result[4];
		remote5 = result[5];
		remote6 = result[6];
		remote7 = result[7];
		remote8 = result[8];
		remote9 = result[9];
	},
	timeout: 10000
});
client.open('GET', Alloy.CFG.url + 'app/data.txt');
client.send();

$.calculate1.addEventListener('click', function() {
	
	$.h.value = $.h.value.replace(',', '.');
	$.w.value = $.w.value.replace(',', '.');
	$.w2.value = $.w2.value.replace(',', '.');
	$.f.value = $.f.value.replace(',', '.');
	$.a.value = $.a.value.replace(',', '.');
	
	if (OS_ANDROID) {
		value1 = $.picker_df1.getSelectedRow(0).value;
		value2 = $.picker_df2.getSelectedRow(0).value;
		value3 = $.picker_tpl.getSelectedRow(0).value;
		value4 = $.picker_tpr.getSelectedRow(0).value;
	}
	// var elipsoide = 1/6 * Math.PI * $.h.value * $.w.value * $.w.value;
	var elipsoide = 1/6 * Math.PI * $.h.value * $.w.value * $.w2.value;
	var elipsoide_gaps = 1/6 * Math.PI * $.h.value * $.w.value * $.a.value;
	var densidad = 10000 / ($.f.value * $.a.value);
	var arbol_ha = elipsoide * densidad;
	var arbol_ha_gaps = elipsoide_gaps * densidad;
	
	var df = null;
	
	switch (value1) {
		case 1:
			switch (value2) {
				case 1:
					df = remote1;
					break;
				case 2:
					df = remote2;
					break;
				case 3:
					df = remote3;
					break;
			}
			break;
		case 2:
			switch (value2) {
				case 1:
					df = remote4;
					break;
				case 2:
					df = remote5;
					break;
				case 3:
					df = remote6;
					break;
			}
			break;
		case 3:
			switch (value2) {
				case 1:
					df = remote7;
					break;
				case 2:
					df = remote8;
					break;
				case 3:
					df = remote9;
					break;
			}
			break;
	}
	
	var vegetacion_total = arbol_ha * 2 * df;
	var vegetacion_total_gaps = arbol_ha_gaps * 2 * df;

	var vegetacion_a_tratar = vegetacion_total * value3; //value3 = tpl
	var vegetacion_a_tratar_gaps = vegetacion_total_gaps * value3; //value3 = tpl

	var aplicacion_teorico = vegetacion_a_tratar * value4;
	var aplicacion_teorico_gaps = vegetacion_a_tratar_gaps * value4;

	// var aplicacion_real = aplicacion_teorico * aux;
	// var aplicacion_real_gaps = aplicacion_teorico_gaps * aux;
	var aplicacion_real = aplicacion_teorico / aux;
	var aplicacion_real_gaps = aplicacion_teorico_gaps / aux;

	var nota = 'Comprobar en la ficha técnica del producto si está autorizado para este uso y si existen límites máximos de volumen de aplicación';

    var ahorro_caldo = ((aplicacion_real_gaps - aplicacion_real) / aplicacion_real_gaps) * 100;

	if (typeof value3 == 'string' || typeof value4 == 'string') {
		$.result1.text = 'Sin volumen recomendado';
		if (typeof value3 == 'string') {
			$.result1.text += '\n\n -' + title3 + ':\n' + value3;
		}
		if (typeof value4 == 'string') {
			$.result1.text += '\n\n -' + title4 + ':\n' + value4;
		}
	} else {
		if (value3 == 34 && value4 == 158) {
	        $.result1.text = 'Tratamiento cebo contra la mosca de la fruta en parcheo en la cara sur de los árboles';
	    } else {
	        if ($.a.value <= $.w2.value) {
	            $.result1.text = 'Volumen recomendado: ' + Math.round(aplicacion_real / 10) * 10 + ' l/ha'
	                + '\n\n' + nota;
	        } else {
	            $.result1.text = 'Volumen recomendado en pulverización contínua: ' + Math.round(aplicacion_real_gaps / 10) * 10 + ' l/ha'
	                + '\n\nVolumen recomendado en pulverización con detección de árboles: ' + Math.round(aplicacion_real / 10) * 10 + ' l/ha'
	                + '\n\nAhorro de caldo en la pulverización con detección de árboles (%): ' + Math.round(ahorro_caldo / 10) * 10 + ' %\n\n'
	                + nota;
	        }
	    }
	}

	$.result_view1.height = Ti.UI.SIZE;

	toSave1 = {
		value1:OS_IOS ? data1[index1].title : $.picker_df1.getSelectedRow(0).title,
		value2:OS_IOS ? data2[index2].title : $.picker_df2.getSelectedRow(0).title,
		value3:OS_IOS ? (data3[index3] ? data3[index3].title : '') : $.picker_tpl.getSelectedRow(0).title,
		value4:OS_IOS ? (data4[index4] ? data4[index4].title : '') : $.picker_tpr.getSelectedRow(0).title,
		h:$.h.value,
		w:$.w.value,
		w2:$.w2.value,
		f:$.f.value,
		a:$.a.value,
		result1:Math.round(aplicacion_teorico / 10) * 10,
		result2:Math.round(aplicacion_real / 10) * 10,
		type:1
	};
	
});

var toSave1;

$.save1.addEventListener('click', function() {
	var win = Alloy.createController('caldo/saving', toSave1).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});

$.calculate2.addEventListener('click', function() {
	
	$.vc.value = $.vc.value.replace(',', '.');
	$.va.value = $.va.value.replace(',', '.');
	$.ac.value = $.ac.value.replace(',', '.');
	$.ba.value = $.ba.value.replace(',', '.');
	
	var nota = 'Seleccionar las boquillas que proporcionen este caudal a las presiones recomendadas (8 - 15).';
	$.result2.text = 'Caudal boquilla = ' + (Math.round(($.vc.value * $.va.value * $.ac.value / 600) / $.ba.value * 10) / 10) + ' l/min\n\n' + nota;
	$.result_view2.height = Ti.UI.SIZE;
	
	toSave2 = {
		value1:$.vc.value,
		value2:$.va.value,
		value3:$.ac.value,
		value4:$.ba.value,
		result1:Math.round(($.vc.value * $.va.value * $.ac.value / 600) / $.ba.value * 10) / 10,
		type:2
	};
});

var toSave2;

$.save2.addEventListener('click', function() {
	var win = Alloy.createController('caldo/saving', toSave2).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});

$.calculate3.addEventListener('click', function() {
	
	$.cb1.value = $.cb1.value.replace(',', '.');
	$.cb2.value = $.cb2.value.replace(',', '.');
	$.cb3.value = $.cb3.value.replace(',', '.');
	$.cb4.value = $.cb4.value.replace(',', '.');
	
	$.nb1.value = $.nb1.value.replace(',', '.');
	$.nb2.value = $.nb2.value.replace(',', '.');
	$.nb3.value = $.nb3.value.replace(',', '.');
	$.nb4.value = $.nb4.value.replace(',', '.');
	
	$.vda.value = $.vda.value.replace(',', '.');
	$.adc.value = $.adc.value.replace(',', '.');
	
	var v2 = 0,
		v3 = 0,
		v4 = 0;
	var v1 = $.nb1.value * $.cb1.value;
	if ($.nb2.value) {
		v2 = $.nb2.value * $.cb2.value;
	}
	if ($.nb3.value) {
		v3 = $.nb3.value * $.cb3.value;
	}
	if ($.nb4.value) {
		v4 = $.nb4.value * $.cb4.value;
	}
	$.result3.text = 'Volumen aplicado = ' + Math.round((600 * (v1 + v2 + v3 + v4) / ($.vda.value * $.adc.value)) / 10) * 10 + ' l/ha';
	$.result_view3.height = Ti.UI.SIZE;
	
	toSave3 = {
		h:$.vda.value,
		w:$.adc.value,
		value1:$.name1.value + '[-]' + $.nb1.value + '[-]' + $.cb1.value,
		value2:$.name2.value + '[-]' + $.nb2.value + '[-]' + $.cb2.value,
		value3:$.name3.value + '[-]' + $.nb3.value + '[-]' + $.cb3.value,
		value4:$.name4.value + '[-]' + $.nb4.value + '[-]' + $.cb4.value,
		result1:Math.round((600 * (v1 + v2 + v3 + v4) / ($.vda.value * $.adc.value)) / 10) * 10,
		type:3
	};
});

var toSave3;

$.save3.addEventListener('click', function() {
	var win = Alloy.createController('caldo/saving', toSave3).getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});

$.volumen_aplicacion.addEventListener('singletap', function(e) {
	this.opacity = .3;
	this.animate({opacity:1});
	$.calc2.height = 0;
	$.calc3.height = 0;
	$.eleccion_boquillas.backgroundColor = Alloy.CFG.green;
	$.volumen_turbo.backgroundColor = Alloy.CFG.green;
	if (this.backgroundColor == Alloy.CFG.green) {
		$.calc1.height = Ti.UI.SIZE;
		this.backgroundColor = Alloy.CFG.orange;
	} else {
		$.calc1.height = 0;
		this.backgroundColor = Alloy.CFG.green;
	}
});
$.eleccion_boquillas.addEventListener('singletap', function(e) {
	this.opacity = .3;
	this.animate({opacity:1});
	$.calc1.height = 0;
	$.calc3.height = 0;
	$.volumen_aplicacion.backgroundColor = Alloy.CFG.green;
	$.volumen_turbo.backgroundColor = Alloy.CFG.green;
	if (this.backgroundColor == Alloy.CFG.green) {
		$.calc2.height = Ti.UI.SIZE;
		this.backgroundColor = Alloy.CFG.orange;
	} else {
		$.calc2.height = 0;
		this.backgroundColor = Alloy.CFG.green;
	}
});
$.volumen_turbo.addEventListener('singletap', function(e) {
	this.opacity = .3;
	this.animate({opacity:1});
	$.calc2.height = 0;
	$.calc1.height = 0;
	$.eleccion_boquillas.backgroundColor = Alloy.CFG.green;
	$.volumen_aplicacion.backgroundColor = Alloy.CFG.green;
	if (this.backgroundColor == Alloy.CFG.green) {
		$.calc3.height = Ti.UI.SIZE;
		this.backgroundColor = Alloy.CFG.orange;
	} else {
		$.calc3.height = 0;
		this.backgroundColor = Alloy.CFG.green;
	}
});

if (OS_ANDROID) {
	/*
	$.h.addEventListener('singletap', function f(e) {
		$.h.softKeyboardOnFocus = Ti.UI.Android.SOFT_KEYBOARD_DEFAULT_ON_FOCUS;
		$.h.removeEventListener('singletap', f);
		$.h.focus();
	});
	*/
	$.h.addEventListener('focus', function f(e) {
		this.blur();
		this.removeEventListener('focus', f);
	});
} else {
	// Funciones de formulario, tecla NEXT
	$.h.addEventListener('return', function() {
		$.w.focus();
	});
	$.w.addEventListener('return', function() {
		$.f.focus();
	});
	$.f.addEventListener('return', function() {
		$.a.focus();
	});
	
	$.vc.addEventListener('return', function() {
		$.va.focus();
	});
	$.va.addEventListener('return', function() {
		$.ac.focus();
	});
	$.ac.addEventListener('return', function() {
		$.ba.focus();
	});
	
	$.vda.addEventListener('return', function() {
		$.adc.focus();
	});
	$.name1.addEventListener('return', function() {
		$.nb1.focus();
	});
	$.nb1.addEventListener('return', function() {
		$.cb1.focus();
	});
	$.name2.addEventListener('return', function() {
		$.nb2.focus();
	});
	$.nb2.addEventListener('return', function() {
		$.cb2.focus();
	});
	$.name3.addEventListener('return', function() {
		$.nb3.focus();
	});
	$.nb3.addEventListener('return', function() {
		$.cb3.focus();
	});
	$.name4.addEventListener('return', function() {
		$.nb4.focus();
	});
	$.nb4.addEventListener('return', function() {
		$.cb4.focus();
	});
}

$.view_saved.addEventListener('singletap', function() {
	var win = Alloy.createController('caldo/saved').getView();
	Alloy.CFG.wins.push(win);
	win.open({left:0});
});
