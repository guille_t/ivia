
var current_win = null;

exports.setWindow = function(win) {
	current_win = win;
};
exports.setTitle = function(text) {
	$.win_title.text = text;
};

exports.setBgColor = function(color, color2) {
	$.header.backgroundColor = color;
	$.win_title.color = color2;
};

$.back.addEventListener('singletap', function() {
	Alloy.CFG.wins.pop();
	current_win.close({left:'100%'});
});
$.home.addEventListener('singletap', function() {
	Alloy.CFG.wins.pop();
	for (var i in Alloy.CFG.wins) {
		Alloy.CFG.wins[i].close();
	}
	Alloy.CFG.wins = [];
	current_win.close({left:'100%'});
});
