var args = arguments[0] || {};

$.header.setWindow($.win);
if (args.item_id) {
	$.header.setTitle('Integral térmica');
} else {
	$.header.setTitle('Alternaria');
}

$.header.setBgColor('#90EBEBEB', '#333');

var getCoords = require('get_coords');
$.win.addEventListener('open', function() {
	new getCoords(setCoords);
});

$.reload.addEventListener('singletap', function() {
	$.reload.hide();
	$.loading.show();
	new getCoords(setCoords);
});

$.loading.show();

function setCoords(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.reload.show();
		$.loading.hide();
		return;
	}
	
	var rows = [];
	
	for (var i = 0; i < data.rows.length; i ++) {
		var a = Alloy.Globals.Map.createAnnotation({
			latitude:data.rows[i].lat,
			longitude:data.rows[i].lng,
			title:data.rows[i].name,
			_id:data.rows[i].id,
			rightButton:OS_IOS ? Ti.UI.iOS.SystemButton.INFO_LIGHT : ''//'/images/open.png'
		});
		
		$.map.addAnnotation(a);
	}
	var lat = ((parseInt(data.max_v) + parseInt(data.min_v)) / 2);
	var lng = ((parseInt(data.max_h) + parseInt(data.min_h)) / 2);
	var diff_lat = (data.max_h - data.min_h) * 1.1;
	var diff_lng = (data.max_v - data.min_v) * 1.1;
	$.map.region = {latitude:lat + 1, longitude:lng, latitudeDelta:diff_lat, longitudeDelta:diff_lng};
	
	$.loading.hide();
	
}

$.map.addEventListener('click', function(e) {
	//if (OS_IOS && e.clicksource == 'rightButton' || OS_ANDROID && e.clicksource == 'title') {
	if (OS_IOS && e.clicksource == 'rightButton' || OS_ANDROID && e.clicksource == 'infoWindow' || e.clicksource == 'title' || e.clicksource == 'subtitle') {
		var graph = Alloy.createController('graph', {a:e.annotation, item_id:args.item_id}).getView();
		Alloy.CFG.wins.push(graph);
		graph.open({left:0});
	}
});
