$.header.setWindow($.win);
$.header.setTitle('Avisos');
$.header.setBgColor('#EEE', '#333');

$.win.addEventListener('open', function() {
	$.name.focus();
});

$.name.addEventListener('click', function() {
	$.name.focus();
});
$.surname.addEventListener('click', function() {
	$.surname.focus();
});
$.email.addEventListener('click', function() {
	$.email.focus();
});
$.org.addEventListener('click', function() {
	$.org.focus();
});

$.message.ok = 'Vale';

var sendData = require('register');

function ok(data) {
	
	if (data == null) {
		$.message.title = 'Error de conexión';
		$.message.message = 'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde';
		$.message.show();
		$.loading.hide();
		$.send.show();
		return;
	}
	
	$.message.title = 'Suscripción realizada';
	$.message.message = 'La suscripción ha sido realizada correctamente';
	$.message.show();
	$.send.show();
	
	$.loading.hide();
	$.send.show();
	
	setTimeout(function() {
		$.win.close({left:'100%'});
	}, 300);
	
}

$.send.addEventListener('click', function() {
	if ($.name.value && $.surname.value && $.email.value && $.org.value) {
		$.send.hide();
		$.loading.show();
		new sendData(ok, {name:$.name.value, surname: $.surname.value, email: $.email.value, org: $.org.value});
	} else {
		$.message.title = 'Error en el formulario';
		$.message.message = 'Debes rellenar todos los campos del formulario antes de enviarlo';
		$.message.show();
	}
});
