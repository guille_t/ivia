$.header.setWindow($.win);
$.header.setTitle('Mi galería');

$.header.setBgColor('#EEE', '#333');

$.take_photo.addEventListener('singletap', function() {
	$.take_photo.opacity = .3;
	$.take_photo.animate({opacity:.8});
	
	Ti.Media.showCamera({
		mediaTypes:[Ti.Media.MEDIA_TYPE_PHOTO],
		success: function(e) {
			success(e);
		},
		cancel: function() {
			
		},
		error: function(e) {
			var error = Ti.UI.createAlertDialog({
				ok:'Vale',
				title:'Error',
				message:'Ha ocurrido un error con la cámara'
			});
			error.show();
		},
		allowEditing:true,
		saveToPhotoGallery:false
	});
});

var file = null,
	file_name = null;
	
// var ImageFactory = require('ti.imagefactory');

function success(e) {
	if (OS_IOS) {
		hint.show();
	}
	$.modify.hide();
	$.accept.show();
	$.textarea.value = '';
	$.photo_text.show();
	setTimeout(function() {
		$.textarea.focus();
	}, 300);
	
	var now = new Date().getTime();
	
	file = Ti.Filesystem.getFile(Ti.Filesystem.applicationDataDirectory + now + '.jpg');
	// var thumb = ImageFactory.imageAsThumbnail(e.media,
	// 	{ size:600, cornerRaduis:0, format: ImageFactory.PNG }
	// );
	// file.write(thumb);
	file.write(e.media);
	
	file_name = file.nativePath;
}

Alloy.Collections.photos.fetch();

$.cancel.addEventListener('click', function(e) {
	$.photo_text.hide();
	$.textarea.blur();
});
$.accept.addEventListener('click', function(e) {
	$.photo_text.hide();
	$.textarea.blur();
	var photo = Alloy.createModel('photos', {path:file_name, text:$.textarea.value});
	photo.save();
	Alloy.Collections.photos.fetch();
	$.table.scrollToIndex($.table.sections[0].rows.length - 1);
});
$.modify.addEventListener('click', function() {
	$.photo_text.hide();
	$.textarea.blur();
	var table = Alloy.createCollection("photos");
	table.fetch({query:"SELECT * FROM photos where id = " + $.modify._id });
	table.at(0).set('text', $.textarea.value);
	table.at(0).save();
	Alloy.Collections.photos.fetch();
});

$.table.addEventListener('click', function(e) {
	
	if (e.source._delete) {
		
		var confirm = Ti.UI.createAlertDialog({
			title:'Eliminar foto',
			message:'¿Seguro que deseas eliminar esta foto?',
			buttonNames:['Sí', 'No'],
			cancel:1
		});
		
		confirm.show();
		
		confirm.addEventListener('click', function(e2) {
			
			if (e2.source.cancel !== e2.index) {
				var table = Alloy.createCollection("photos");
				table.fetch({query:"SELECT * FROM photos where id = " + e.row.rowId });
			    if (table.length > 0) {
					var file_to_delete = Ti.Filesystem.getFile(table.at(0).get('path'));
					if (file_to_delete.exists()) {
						file_to_delete.deleteFile();
					}
					table.at(0).destroy();
					Alloy.Collections.photos.fetch();
				}
			}
			
		});
		
	} else if (e.source._edit) {
		$.modify._id = e.row.rowId;
		$.modify.show();
		$.accept.hide();
		if (OS_IOS) {
			if (e.row._text != '') {
				hint.hide();
			} else {
				hint.show();
			}
		}
		$.textarea.value = e.row._text;
		$.photo_text.show();
		setTimeout(function() {
			$.textarea.focus();
		}, 300);
	} else if (e.source._image) {
		if (OS_ANDROID) {
			var big_view = Ti.UI.createView({
				backgroundColor:'#9000',
				zIndex:200
			});
			var img = Ti.UI.createImageView({
				image:e.source.image,
				enableZoomControls:true
			});
		} else {
			var big_view = Ti.UI.createScrollView({
				backgroundColor:'#9000',
				maxZoomScale:10,
				minZoomScale:1,
				zIndex:200
			});
			var img = Ti.UI.createImageView({
				image:e.source.image,
				width:'100%',
				defaultImage:'none'
			});
		}
		var loader = Ti.UI.createActivityIndicator();
		loader.show();
		big_view.add(img);
		big_view.add(loader);
		img.addEventListener('load', function() {
			loader.hide();
		});
		big_view.addEventListener('singletap', function() {
			big_view.parent.remove(big_view);
		});
		$.win.add(big_view);
	} else if (e.source._share) {
		var file = Ti.Filesystem.getFile(e.row._path);
		var email = Ti.UI.createEmailDialog({
			subject:'Te han compartido esta foto',
			html:true,
			toRecipients:null,
			messageBody:'Foto de IVIA'
		});
		email.addAttachment(file);
		email.open();
	}
	
});

if (OS_IOS) {
	var hint = Ti.UI.createLabel({
		text: $.textarea.hintText,
		color:'#BBB',
		font:{fontSize:Alloy.CFG.fontSize, fontFamily:Alloy.CFG.fontFamily},
		top:'10dp',
		left:'10dp',
		visible:true
	});
	$.textarea.add(hint);
	$.textarea.addEventListener('change', function() {
		if ($.textarea.value == '') {
			hint.show();
		} else {
			hint.hide();
		}
	});
} else {
	$.take_photo.borderRadius = 30 * (Ti.Platform.displayCaps.dpi / 160);
}
