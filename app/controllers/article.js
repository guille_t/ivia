var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.title);

/*
if (OS_ANDROID) {
    var interval = setInterval(function() {
        $.scrollableview.moveNext();
    }, 2000);
}
*/

$.header.setBgColor('#6000', '#FFF');

var getArticle = require('get_article');
new getArticle(setArticle, args.id);

$.reload.addEventListener('singletap', function() {
    $.reload.hide();
    $.loading.show();
    new getArticle(setArticle, args.id);
});

$.loading.show();

var current_data = null;

var haveIt = false;
var favorites = Ti.App.Properties.getList('favorites', []);

function setArticle(data) {

    if (data == null) {
        Ti.UI.createAlertDialog({
            title:'Error de conexión',
            message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
            ok:'Vale'
        }).show();
        $.reload.show();
        $.loading.hide();
        return;
    }

    current_data = data;

    for (var i = 0; i < favorites.length; i ++) {
        if (favorites[i] == args.id) {
            haveIt = true;
            $.star.image = '/images/star_green.png';
        }
    }

    var images = data.images;

    for (var i in images) {

        // var image = Alloy.createController('article/image', {image:images[i].image, id:args.id + '-' + i}).getView();
        var image = Alloy.createController('article/image_module', {image:images[i].image}).getView();

        var view = Ti.UI.createView({
            _big:images[i].image
        });

        var dark = Ti.UI.createView({
            backgroundColor:'#6000',
            height:'50dp',
            bottom:OS_IOS ? '20dp' : 0,
            touchEnabled:false
        });

        dark.add(Ti.UI.createLabel({
            left:'10dp',
            right:'10dp',
            textAlign:'center',
            color:'#FFF',
            shadowColor:'#333',
            shadowOffset:{x:1,y:1},
            font:{fontFamily:Alloy.CFG.fontFamily, fontSize:Alloy.CFG.fontSize},
            text:images[i].title,
            touchEnabled:false
        }));

        view.add(image);
        view.add(dark);

        $.scrollableview.addView(view);

        view.addEventListener('singletap', function(e) {
            if (OS_ANDROID) {
                var big_view = Ti.UI.createView({
                    backgroundColor:'#9000',
                    zIndex:200
                });
                var img = Ti.UI.createImageView({
                    image:this._big,
                    enableZoomControls:true
                });
            } else {
                var big_view = Ti.UI.createScrollView({
                    backgroundColor:'#9000',
                    maxZoomScale:10,
                    minZoomScale:1,
                    zIndex:200
                });
                var img = Ti.UI.createImageView({
                    image:this._big,
                    width:'100%',
                    defaultImage:'none'
                });
            }
            var loader = Ti.UI.createActivityIndicator();
            loader.show();
            big_view.add(img);
            big_view.add(loader);
            img.addEventListener('load', function() {
                loader.hide();
            });
            big_view.addEventListener('singletap', function() {
                big_view.parent.remove(big_view);
            });
            $.win.add(big_view);

        });

    }

    if (args.post == 1) {
        $.left_tab_image.image = '/images/search.png';
        $.left_tab_txt.color = '#666';
        $.center_tab_image.image = '/images/muestreo2.png';
        $.center_tab_txt.color = '#A2B139';
        $.right_tab_image.image = '/images/control.png';
        $.right_tab_txt.color = '#666';
    }
    if (args.post == 2) {
        $.left_tab_image.image = '/images/search.png';
        $.left_tab_txt.color = '#666';
        $.center_tab_image.image = '/images/muestreo.png';
        $.center_tab_txt.color = '#666';
        $.right_tab_image.image = '/images/control2.png';
        $.right_tab_txt.color = '#A2B139';
    }

    $.title.text = data.posts[args.post].post_title;
    var wv = Ti.UI.createWebView({
        html: Alloy.Globals.getHTML(data.posts[args.post].post_content),
        top:'10dp',
        right:'10dp',
        left:'10dp',
        height:Ti.UI.SIZE,
        backgroundColor:'transparent'
    });
    $.scrollview.add(wv);

    // wv.html = Alloy.Globals.getHTML(data.posts[args.post].post_content);

    $.loading.hide();
    $.scrollview.animate({opacity:1});

    $.bg_logo.hide();
    $.bg_gip.hide();

}

if (Ti.Platform.displayCaps.platformWidth > 640) {
    // $.center_tab.width = $.right_tab.width = $.left_tab.width = (parseInt($.left_tab.width) * 1.125) + 'dp';
}

var ids1 = [4915];
var ids2 = [1808, 516, 1801, 1820]; // Piojo rojo, Cotonet, Piojo gris, Serpeta gruesa

function inArray(needle, haystack) {
    var length = haystack.length;
    for(var i = 0; i < length; i++) {
        if(haystack[i] == needle) return true;
    }
    return false;
}

$.left_tab.addEventListener('singletap', function() {
    $.left_tab_image.image = '/images/search2.png';
    $.left_tab_txt.color = '#A2B139';
    $.center_tab_image.image = '/images/muestreo.png';
    $.center_tab_txt.color = '#666';
    $.right_tab_image.image = '/images/control.png';
    $.right_tab_txt.color = '#666';
    $.new_tab_image.image = '/images/chufas.png';
    $.new_tab_txt.color = '#666';
    $.title.text = current_data.posts[0].post_title;
    // $.scrollview.remove(wv);
    var children = $.scrollview.children;
    $.scrollview.remove(children[children.length - 1]);
    var wv = Ti.UI.createWebView({
        html: Alloy.Globals.getHTML(current_data.posts[0].post_content),
        top:'10dp',
        right:'10dp',
        left:'10dp',
        height:Ti.UI.SIZE,
        backgroundColor:'transparent'
    });
    $.scrollview.add(wv);
    // wv.html = Alloy.Globals.getHTML(current_data.posts[0].post_content);
    //if (current_data.posts[1].ID == 4915) {
    if (inArray(current_data.posts[1].ID, ids1)) {
        $.graph.height = 0;
        $.graph.top = $.graph.bottom = 0;
        $.graph.hide();
    }
    if (inArray(current_data.posts[1].ID, ids2)) {
        $.graph2.height = 0;
        $.graph2.top = $.graph.bottom = 0;
        $.graph2.hide();
    }
});

$.center_tab.addEventListener('singletap', function() {
    if (current_data.posts[1]) {
        $.left_tab_image.image = '/images/search.png';
        $.left_tab_txt.color = '#666';
        $.center_tab_image.image = '/images/muestreo2.png';
        $.center_tab_txt.color = '#A2B139';
        $.right_tab_image.image = '/images/control.png';
        $.right_tab_txt.color = '#666';
        $.new_tab_image.image = '/images/chufas.png';
        $.new_tab_txt.color = '#666';
        $.title.text = current_data.posts[1].post_title;
        //if (current_data.posts[1].ID == 4915) {
        if (inArray(current_data.posts[1].ID, ids1)) {
            $.graph.height = '40dp';
            $.graph.top = '20dp';
            $.graph.bottom = '0';
            $.graph.show();
        }
        if (inArray(current_data.posts[1].ID, ids2)) {
            $.graph2.height = '40dp';
            $.graph2.top = '20dp';
            $.graph2.bottom = '0';
            $.graph2.show();
        }
        var children = $.scrollview.children;
        $.scrollview.remove(children[children.length - 1]);
        var wv = Ti.UI.createWebView({
            html: Alloy.Globals.getHTML(current_data.posts[1].post_content),
            top:'10dp',
            right:'10dp',
            left:'10dp',
            height:Ti.UI.SIZE,
            backgroundColor:'transparent'
        });
        $.scrollview.add(wv);
        // wv.html = Alloy.Globals.getHTML(current_data.posts[1].post_content);
    }
});

$.right_tab.addEventListener('singletap', function() {
    if (current_data.posts[2]) {
        $.left_tab_image.image = '/images/search.png';
        $.left_tab_txt.color = '#666';
        $.center_tab_image.image = '/images/muestreo.png';
        $.center_tab_txt.color = '#666';
        $.right_tab_image.image = '/images/control2.png';
        $.right_tab_txt.color = '#A2B139';
        $.new_tab_image.image = '/images/chufas.png';
        $.new_tab_txt.color = '#666';
        $.title.text = current_data.posts[2].post_title;
        // $.scrollview.remove(wv);
        var children = $.scrollview.children;
        $.scrollview.remove(children[children.length - 1]);
        var wv = Ti.UI.createWebView({
            html: Alloy.Globals.getHTML(current_data.posts[2].post_content),
            top:'10dp',
            right:'10dp',
            left:'10dp',
            height:Ti.UI.SIZE,
            backgroundColor:'transparent'
        });
        $.scrollview.add(wv);
        // wv.html = Alloy.Globals.getHTML(current_data.posts[2].post_content);
        //if (current_data.posts[1].ID == 4915) {
        if (inArray(current_data.posts[1].ID, ids1)) {
            $.graph.height = 0;
            $.graph.top = $.graph.bottom = 0;
            $.graph.hide();
        }
        if (inArray(current_data.posts[1].ID, ids2)) {
            $.graph2.height = 0;
            $.graph2.top = $.graph.bottom = 0;
            $.graph2.hide();
        }
    }
});

$.new_tab.addEventListener('singletap', function() {
    if (current_data.posts[3]) {
        $.left_tab_image.image = '/images/search.png';
        $.left_tab_txt.color = '#666';
        $.center_tab_image.image = '/images/muestreo.png';
        $.center_tab_txt.color = '#666';
        $.right_tab_image.image = '/images/control.png';
        $.right_tab_txt.color = '#666';
        $.new_tab_image.image = '/images/chufas2.png';
        $.new_tab_txt.color = '#A2B139';
        $.title.text = current_data.posts[3].post_title;
        // $.scrollview.remove(wv);
        var children = $.scrollview.children;
        $.scrollview.remove(children[children.length - 1]);
        var wv = Ti.UI.createWebView({
            html: Alloy.Globals.getHTML(current_data.posts[3].post_content),
            top:'10dp',
            right:'10dp',
            left:'10dp',
            height:Ti.UI.SIZE,
            backgroundColor:'transparent'
        });
        $.scrollview.add(wv);
        // wv.html = Alloy.Globals.getHTML(current_data.posts[3].post_content);
        //if (current_data.posts[1].ID == 4915) {
        if (inArray(current_data.posts[1].ID, ids1)) {
            $.graph.height = 0;
            $.graph.top = $.graph.bottom = 0;
            $.graph.hide();
        }
        if (inArray(current_data.posts[1].ID, ids2)) {
            $.graph2.height = 0;
            $.graph2.top = $.graph.bottom = 0;
            $.graph2.hide();
        }
    }
});

/*
var favorites = Alloy.Collections.favorites;

favorites.map(function(fav) {
    alert(fav.get('article_id'));
});
*/

$.fav.addEventListener('singletap', function() {
    /*
    var fav = Alloy.createModel('favorites', {article_id:args.id});
    favorites.add(fav);
    fav.save();
    */
    if (haveIt) {
        $.star.image = '/images/star_white.png';
        var aux = [];
        for (var i = 0; i < favorites.length; i ++) {
            if (favorites[i] != args.id) {
                aux.push(favorites[i]);
            }
        }
        Ti.App.Properties.setList('favorites', aux);
        haveIt = false;
    } else {
        favorites.push(args.id);
        Ti.App.Properties.setList('favorites', favorites);
        $.star.image = '/images/star_green.png';
        haveIt = true;
    }
});

$.graph.addEventListener('singletap', function(e) {
    $.graph.opacity = .3;
    $.graph.animate({opacity:1});
    var map = Alloy.createController('map').getView();
    Alloy.CFG.wins.push(map);
    map.open({left:0});
});

$.graph2.addEventListener('singletap', function(e) {
    $.graph2.opacity = .3;
    $.graph2.animate({opacity:1});
    var map = Alloy.createController('map', {item_id:args.id}).getView();
    Alloy.CFG.wins.push(map);
    map.open({left:0});
});
