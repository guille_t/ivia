var args = arguments[0] || {};

$.row._title = $.title.text = args.title;

// var ImageFactory = require('ti.imagefactory');

$.row._id = args.id;

$.row._row_view = $.row_view;

var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory + args.id + '__row.jpg');

if (!file.exists()) {
	$.loading.show();
	var client = Ti.Network.createHTTPClient({
		timeout:15000,
		onload:function() {
			var width = 720;
			var height = 292;
			// var thumb = ImageFactory.imageTransform(client.responseData,
			// 	{ type:ImageFactory.TRANSFORM_CROP, width:width, height:height },
			// 	{ type:ImageFactory.TRANSFORM_ROUNDEDCORNER, borderSize:0, cornerRadius:0 }
			// );
			// file.write(thumb);
			// $.image.image = thumb;
			file.write(client.responseData);
			$.image.image = client.responseData;
			$.image.height = Ti.UI.SIZE;
			$.row.height = Ti.UI.SIZE;
			$.loading.text = '';
			$.loading.hide();
		},
		onerror:function(e) {
			$.loading.text = 'Error';
		},
		ondatastream:function(e) {
			$.loading.text = Math.round(e.progress * 100) + ' %';
		},
		timeout:10000
	});
	client.open('GET', args.url.src);
	client.send();
} else {
	$.image.image = file.nativePath;
}
