var args = arguments[0] || {};

$.square.left = args.left;
$.square.top = args.top;
$.square._id = args.data.id;
$.square._title = args.data.title;

// var ImageFactory = require('ti.imagefactory');

var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory + args.data.id + '_square.jpg');

if (!file.exists()) {
	var client = Ti.Network.createHTTPClient({
		timeout:150000,
		onload:function() {
			var width = 200;
			var height = 200;
			if (Ti.Platform.displayCaps.platformWidth > 640) {
				width *= 1.25;
				height *= 1.25;
			}
			// var thumb = ImageFactory.imageAsThumbnail(client.responseData,
				// { size:width, cornerRaduis:0, format: ImageFactory.PNG }
			// );
			// file.write(thumb);
			// $.image.image = thumb;
			file.write(client.responseData);
			$.image.image = client.responseData;
		},
		onerror:function(e) {
		},
		ondatastream:function(e) {
		},
		timeout:10000
	});
	client.open('GET', args.data.url.src);
	client.send();
} else {
	$.image.image = file.nativePath;
}

$.title.text = args.data.title;
