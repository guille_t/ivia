var args = arguments[0] || {};

// var ImageFactory = require('ti.imagefactory');

var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory + args.id + '__article2.jpg');

if (!file.exists()) {
	$.loading.show();
	var client = Ti.Network.createHTTPClient({
		timeout:150000,
		onload:function() {
			var width = 720;
			if (Ti.Platform.displayCaps.platformWidth > 640) {
				var height = 471;
			} else {
				var height = 530;
			}
			// var thumb = ImageFactory.imageTransform(client.responseData,
				// { type:ImageFactory.TRANSFORM_CROP, width:width, height:height },
				// { type:ImageFactory.TRANSFORM_ROUNDEDCORNER, borderSize:0, cornerRadius:0 }
			// );
			// file.write(thumb);
			// $.img.image = thumb;
			file.write(client.responseData)
			$.img.image = client.responseData;
			$.loading.hide();
		},
		onerror:function(e) {
		},
		ondatastream:function(e) {
		},
		timeout:10000
	});
	client.open('GET', args.image);
	client.send();
} else {
	$.img.image = file.nativePath;
}
