var args = arguments[0] || {};
$.title.text = args.title;
$.text.text = args.intro;

$.row._view = $.row_view;

// var ImageFactory = require('ti.imagefactory');

var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory + args.id + '_new.jpg');

if (!file.exists()) {
	var client = Ti.Network.createHTTPClient({
		timeout:150000,
		onload:function() {
			if (client.responseData.width < 200) {
				var width = 150;
				var height = 150;
			} else {
				var width = 200;
				var height = 200;
			}
			// var thumb = ImageFactory.imageAsThumbnail(client.responseData,
			// 	{ size:width, cornerRaduis:0, format: ImageFactory.PNG }
			// );
			// file.write(thumb);
			file.write(client.responseData);
			$.image.image = client.responseData;
		},
		onerror:function(e) {
		},
		ondatastream:function(e) {
		},
		timeout:10000
	});
	client.open('GET', args.image_small);
	client.send();
} else {
	$.image.image = file.nativePath;
}
