var args = arguments[0] || {};

$.header.setWindow($.win);
$.header.setTitle(args.title);

$.header.setBgColor('#EBEBEB', '#333');

var webview = Ti.UI.createWebView({
	top:'50dp',
	html: Alloy.Globals.getHTML(args.text)
});
$.win.add(webview);
