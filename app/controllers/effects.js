$.header.setWindow($.win);
$.header.setTitle('Efectos secundarios');

var current_table = null;

$.b1.addEventListener('click', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	showPopup('productos');
});
$.b2.addEventListener('click', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	showPopup('enemigos');
});

var getData = require('get_effects');

function showPopup(type) {
	
	current_table = type;
	
	$.loading.show();
	$.table.data = [];
	$.table.hide();
	$.dark.show();
	if (type == 'productos') {
		$.table_title.text = 'Nombre del insecticida';
		var color = Alloy.CFG.orange;
	} else {
		$.table_title.text = 'Enemigo natural';
		var color = Alloy.CFG.green;
	}
	new getData(setData, {type:type});
	$.table.separatorColor = $.table_title.color = $.popup.borderColor = color;
}

function setData(data) {
	
	if (data == null) {
		Ti.UI.createAlertDialog({
			title:'Error de conexión',
			message:'Ha ocurrido un error con la conexión al servidor, inténtelo de nuevo más tarde',
			ok:'Vale'
		}).show();
		$.loading.hide();
		return;
	}
	
	var rows = [];
	
	for (var i = 0; i < data.length; i ++) {

		var row = Ti.UI.createTableViewRow({
			height:'40dp',
			_title:data[i].title,
			//color:'#333',
			//font:{fontSize:Alloy.CFG.fontSize, fontWeight:'normal', fontFamily:Alloy.CFG.fontFamily},
			_id:data[i].id
		});
		row.add(Ti.UI.createLabel({
			left:'10dp',
			right:'40dp',
			height:'20dp',
			text:data[i].title,
			color:'#333',
			font:{fontSize:Alloy.CFG.fontSize, fontWeight:'normal', fontFamily:Alloy.CFG.fontFamily}
		}));
		var checked = Ti.UI.createImageView({
			image:'/images/check.png',
			right:'10dp',
			width:'12dp',
			visible:false
		});

		for (var j in enemies) {
			if (enemies[j].title == data[i].title) {
				checked.show();
				row._selected = true;
			}
		}
		for (var j in products) {
			if (products[j].title == data[i].title) {
				checked.show();
				row._selected = true;
			}
		}

		row.add(checked);
		row._myCheck = checked;
			
		rows.push(row);
		
		row._data = data[i];
		
	}
	
	$.table.appendRow(rows);
	
	$.loading.hide();
	$.table.show();
	
}

var products = [],
	enemies = [];

$.table.addEventListener('click', function(e) {
	if (e.row._selected) {
		e.row._myCheck.hide();
		e.row._selected = false;
		if (current_table == 'productos') {
			var aux = [];
			for (var i in products) {
				if (products[i].id != e.row._id) {
					aux.push({id:products[i].id, title:products[i].title});
				}
			}
			products = aux;
		} else {
			var aux = [];
			for (var i in enemies) {
				if (enemies[i].id != e.row._id) {
					aux.push({id:enemies[i].id, title:enemies[i].title});
				}
			}
			enemies = aux;
		}	
	} else {
		e.row._myCheck.show();
		e.row._selected = true;
		if (current_table == 'productos') {
			products.push({id:e.row._id, title:e.row._title});
		} else {
			enemies.push({id:e.row._id, title:e.row._title});
		}
	}
	
});

$.accept.addEventListener('singletap', function(e) {
	$.dark.hide();
	showing = null;
	go();
	if (products.length) {
		$.selected1.text = '';
		for (var i in products) {
			$.selected1.text +=  '\n\n' + products[i].title;
		}
	}
	if (enemies.length) {
		$.selected2.text = '';
		for (var i in enemies) {
			$.selected2.text += '\n\n' + enemies[i].title;
		}
	}
});

function go() {
	if (products.length && enemies.length) {
		$.loading_result.show();
		new getData(setResult, {products:products, enemies:enemies});
	}
}

function setResult(data) {
	$.result_text.text = '';
	for (var i in data) {
		$.result_text.text += data[i] + '\n\n';
	}
	
	$.loading_result.hide();
}
