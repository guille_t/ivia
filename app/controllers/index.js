setTimeout(function() {
	$.win.open();
	$.win.isOpened = true;
	$.scrollview.animate({opacity:1, delay:100});
}, 1000);

$.win.isOpened = false;

$.win.addEventListener('open', function() {
	if (!Ti.Media.hasCameraPermissions()) {
		Ti.Media.requestCameraPermissions(function(e) {
			if (e.success) {
				Ti.API.info('You were granted permission.');
			} else {
				Ti.API.info('ou were denied permission for now, forever or the dialog did not show at all because it was denied forever before.');
			}
		});
	}
	if (!Ti.Geolocation.hasLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE)) {
		Ti.Geolocation.requestLocationPermissions(Ti.Geolocation.AUTHORIZATION_WHEN_IN_USE, function(e) {
			if (e.success) {
				Ti.API.info('You were granted permission.');
			} else {
				Ti.API.info('ou were denied permission for now, forever or the dialog did not show at all because it was denied forever before.');
			}
		});
	}
});

/*
 * Notifications
 */
if (OS_IOS) {
	var Cloud = require('cloud_ios');
} else {
	var Cloud = require('cloud_android');
}

Ti.UI.iOS.appBadge = 0;
Ti.App.addEventListener('resume', function() {
	Ti.UI.iOS.appBadge = 0;
});

new Cloud(notification_callback, $.win);

function notification_callback(data) {
	news = Alloy.createController('news').getView();
	Alloy.CFG.wins.push(news);
	news.open({left:0});
}
/*
 * END notifications
 */

var width = 132;
var height = 132;
var start_h = 80;
var space = 2;
var left_space = '27dp';
if (OS_IOS) {
	var right_space = '27dp';
} else {
	var right_space = '26dp';
}

if (Ti.Platform.displayCaps.platformWidth > 640) {
	height = width = Ti.Platform.displayCaps.platformWidth / 4.7;
	
	if (OS_ANDROID) {
		height = width = width / (Ti.Platform.displayCaps.logicalDensityFactor / 2);
	}
	
	$.b1_image.width = $.b1_image.height = $.b1.width = $.b1.height = width + 'dp';
	$.b2_image.width = $.b2_image.height = $.b2.width = $.b2.height = width + 'dp';
	$.b3_image.width = $.b3_image.height = $.b3.width = $.b3.height = width + 'dp';
	$.b4_image.width = $.b4_image.height = $.b4.width = $.b4.height = width + 'dp';
	$.b5_image.width = $.b5_image.height = $.b5.width = $.b5.height = width + 'dp';
	$.news_image.width = $.news_image.height = $.news.width = $.news.height = width + 'dp';
	$.fav.width = $.reg.width = width + 'dp';
}

$.b1.top = (start_h + (height + space) * 0) + 'dp'; $.b1.left = left_space; $.b1_image.image = '/images/cat1.png';
$.b2.top = (start_h + (height + space) * 0) + 'dp'; $.b2.right = right_space; $.b2_image.image = '/images/cat2.png';
$.b3.top = (start_h + (height + space) * 1) + 'dp'; $.b3.left = left_space; $.b3_image.image = '/images/cat3.png';
$.b4.top = (start_h + (height + space) * 1) + 'dp'; $.b4.right = right_space; $.b4_image.image = '/images/cat4.png';
$.b5.top = (start_h + (height + space) * 2) + 'dp'; $.b5.left = left_space; $.b5_image.image = '/images/cat5.png';
$.news.top = (start_h + (height + space) * 2) + 'dp'; $.news.right = right_space;

//$.fav.top = (5 + start_h + (height + space) * 3) + 'dp'; $.fav.left = left_space;
//$.reg.top = (5 + start_h + (height + space) * 3) + 'dp'; $.reg.right = right_space;
$.footer.top = (10 + start_h + (height + space) * 3) + 'dp';

$.logo_ivia.left = left_space;
$.logo_generalitat.right = right_space;

$.b1_lbl.text = 'Plagas y enfermedades';
$.b2_lbl.text = 'Plagas secundarias';
$.b3_lbl.text = 'Enemigos naturales';
$.b4_lbl.text = 'Efectos secundarios';
$.b5_lbl.text = 'Tratamientos fitosanitarios';
$.news_lbl.text = 'Noticias';

$.fav_txt.text = 'Favoritos';
$.photos_txt.text = 'Mi galería';
$.reg_txt.text = 'Suscripción';
$.fav_image.image = '/images/fav.png';
$.photos_image.image = '/images/photo-grey.png';
$.reg_image.image = '/images/reg.png';

var subcat1 = subcat2 = enemies = effects = treatments = news = null;

$.b1.addEventListener('singletap', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	//if (!subcat1) {
		subcat1 = Alloy.createController('subcat', {id:10, title:$.b1_lbl.text}).getView();
	//}
	Alloy.CFG.wins.push(subcat1);
	subcat1.open({left:0});
});
$.b2.addEventListener('singletap', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	//if (!subcat2) {
		subcat2 = Alloy.createController('subcat', {id:9, title:$.b2_lbl.text}).getView();
	//}
	Alloy.CFG.wins.push(subcat2);
	subcat2.open({left:0});
});
$.b3.addEventListener('singletap', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	//if (!enemies) {
		enemies = Alloy.createController('enemies').getView();
	//}
	Alloy.CFG.wins.push(enemies);
	enemies.open({left:0});
});
$.b4.addEventListener('singletap', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	//if (!effects) {
		effects = Alloy.createController('effects').getView();
	//}
	Alloy.CFG.wins.push(effects);
	effects.open({left:0});
});
$.b5.addEventListener('singletap', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	//if (!treatments) {
		treatments = Alloy.createController('treatments').getView();
	//}
	Alloy.CFG.wins.push(treatments);
	treatments.open({left:0});
});

$.news.addEventListener('singletap', function(e) {
	e.source.opacity = .3;
	e.source.animate({opacity:1});
	//if (!news) {
		news = Alloy.createController('news').getView();
	//}
	Alloy.CFG.wins.push(news);
	news.open({left:0});
});

Alloy.CFG.wins = [];

$.logo_ivia.addEventListener('doubletap', function() {
	var files = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory);
	var aux = files.getDirectoryListing();
	for (i in aux) {
		var file = Ti.Filesystem.getFile(Ti.Filesystem.applicationCacheDirectory + aux[i]);
		if (file.exists()) {
			file.deleteFile();
		}
	}
});

$.fav.width = $.photos.width = $.reg.width = Ti.Platform.displayCaps.platformWidth / 3;

$.reg.addEventListener('singletap', function(e) {
	$.reg.opacity = .3;
	$.reg.animate({opacity:1});
	var reg = Alloy.createController('reg').getView();
	Alloy.CFG.wins.push(reg);
	reg.open({left:0});
});

$.photos.addEventListener('singletap', function(e) {
	$.photos.opacity = .3;
	$.photos.animate({opacity:1});
	var photos = Alloy.createController('photos').getView();
	Alloy.CFG.wins.push(photos);
	photos.open({left:0});
});

$.fav.addEventListener('singletap', function(e) {
	$.fav.opacity = .3;
	$.fav.animate({opacity:1});
	var fav = Alloy.createController('favorites').getView();
	Alloy.CFG.wins.push(fav);
	fav.open({left:0});
});