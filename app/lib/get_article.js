
module.exports = function(f_callback, id) {
	/*
	if (Ti.App.Properties.getString('subcats_' + id, null)) {
		var result = JSON.parse(Ti.App.Properties.getString('subcats_' + id, null));
		f_callback(result.data);
		return;
	}
	*/
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			var result = JSON.parse(this.responseText);
			Ti.API.info('RESULT: ' + this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				f_callback(null);
			}
		},
		onerror: function(e) {
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('GET', Alloy.CFG.url + 'app/article.php?html=1&id=' + id);
	client.send();
};