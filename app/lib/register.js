
module.exports = function(f_callback, args) {
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info('RESULT: ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				f_callback(null);
			}
		},
		onerror: function(e) {
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('POST', Alloy.CFG.url + '/app/suscribe.php');
	client.send({
		/*
		gsom_fname_field:args.name,
		Last_Name:args.surname,
		gsom_email_field:args.email,
		Organizacin:args.org
		*/
		name:args.name,
		surname:args.surname,
		email:args.email,
		org:args.org
	});
};