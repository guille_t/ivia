
module.exports = function(f_callback) {
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			var result = JSON.parse(this.responseText);
			Ti.API.info('RESULT: ' + this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				f_callback(null);
			}
		},
		onerror: function(e) {
			f_callback(null);
		},
		timeout: 10000
	});
	client.open('POST', Alloy.CFG.url + 'app/favorites.php');
	client.send({
		ids:Ti.App.Properties.getList('favorites', []).join(',')
	});
	
};