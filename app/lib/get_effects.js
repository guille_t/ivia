
module.exports = function(f_callback, args) {
	
	var client = Ti.Network.createHTTPClient({
		onload: function() {
			Ti.API.info('RESULT: ' + this.responseText);
			var result = JSON.parse(this.responseText);
			if (result.status == 'ok') {
				f_callback(result.data);
			} else {
				f_callback(null);
			}
		},
		onerror: function(e) {
			f_callback(null);
		},
		timeout: 10000
	});
	
	if (args.type) {
		client.open('GET', Alloy.CFG.url + 'app/effects.php?type=' + args.type);
		client.send();
	} else {
		var p = [];
		for (var i in args.products) {
			p.push(args.products[i].id + ':' + args.products[i].title);
		}
		var e = [];
		for (var i in args.enemies) {
			e.push(args.enemies[i].id + ':' + args.enemies[i].title);
		}
		client.open('POST', Alloy.CFG.url + 'app/effects.php');
		client.send({
			products:p.join(),
			enemies:e.join()
		});
	}
};