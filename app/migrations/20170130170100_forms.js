
migration.up = function(migrator) {
    migrator.db.execute('ALTER TABLE ' + migrator.table + ' ADD COLUMN w2 text;');
};
migration.down = function(migrator) {
    migrator.db.execute('ALTER TABLE ' + migrator.table + ' REMOVE COLUMN w2;');
};
